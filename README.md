# Construct3 Project Template

Template for Construct3 LTS project, including support to export project in CI/CD via Playwright
Web build playable on https://rthery.gitlab.io/construct3-project-template/

## Debug

### Prerequisites

* Node v20 LTS

VSCode has also a nice plugin for Playwright, cf. https://playwright.dev/docs/getting-started-vscode

### Installation

1. Clone the repository
2. Install the node dependencies
    ```sh
    npm install
    ```
3. Install Playwright, you should just use Chromium
    ```sh
    npx playwright install
    ```
4. Zip your project files back to game.c3p at the root of your project
5. Run this command to visualize playwright export the project automatically
    ```sh
    npx playwright test --debug
    ```

## Contributing

Contributions are welcome! Please open an issue or submit a pull request for any changes.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.