/// <reference lib="dom"/>

import { test } from '@playwright/test';
import { readFileSync, writeFile} from 'fs';

let platform = '';
let username = '';
let password = '';

test.describe('Export Construct3 project', () => {
  test.beforeAll(() => {
    platform = process.env.PLATFORM || 'HTML5';
    username = process.env.CONSTRUCT_USERNAME || '';
    password = process.env.CONSTRUCT_PASSWORD || '';

    if (username === '' || password === '') {
      console.error('Missing Construct3 username or password');
      process.exit(1);
    }
  });

  test('Export Construct3 project', async ({ page }) => {
    await page.goto('https://editor.construct.net/lts');
  
    await page.locator('//*[@id="welcomeTourDialog"]/ui-dialog-caption/ui-close-button').click();

    await page.getByTitle('User account').locator('ui-icon').click();
    await page.getByRole('menuitem', { name: 'Log in' }).click();
    await page.waitForSelector('#loginDialog iframe');
    await page.locator('#loginDialog iframe').contentFrame().getByLabel('Username').click();
    await page.locator('#loginDialog iframe').contentFrame().getByLabel('Username').fill(username);
    await page.locator('#loginDialog iframe').contentFrame().getByLabel('Password').click();
    await page.locator('#loginDialog iframe').contentFrame().getByLabel('Password').fill(password);
    password = '';
    await page.locator('#loginDialog iframe').contentFrame().getByRole('button', { name: 'Log in' }).click();

    await page.locator('#loginDialog iframe') === null;
    await page.locator('#userAccountName').textContent() === username;
  
    const documentTitle = await page.title();

    const buffer = readFileSync('./game.c3p').toString('base64');
    const dataTransfer = await page.evaluateHandle(async (data) => {
      const dt = new DataTransfer();
  
      const response = await fetch(data);
      const blobData = await response.blob();
  
      const file = new File([blobData], 'game.c3p', { type: 'application/zip' });
      dt.items.add(file);
      return dt;
    }, 'data:application/octet-stream;base64,' + buffer);

    // Wait until project is loaded, based on the document title
    while (true) {
      await page.waitForTimeout(250);

      await page.dispatchEvent('#main', 'drop', { dataTransfer });
    
      let totalWait = 0;
      let projectLoaded = false;
      while (totalWait < 1000) {
        await page.waitForTimeout(250);
        totalWait += 250;
        const newTitle = await page.$eval('head title', el => el.textContent);
        console.log('Page title:', newTitle);
        projectLoaded = newTitle !== documentTitle;
        if (projectLoaded) {
          break;
        }
      }

      if (projectLoaded) {
        break;
      }
    }

    await page.getByRole('button', { name: 'Menu' }).click();
    await page.getByRole('menuitem', { name: 'Project' }).getByText('Project').click();
    await page.getByRole('menuitem', { name: 'Export' }).click();
    await page.locator('ui-iconviewitem').filter({ hasText: new RegExp(platform) }).locator('ui-icon').click();
    await page.getByRole('button', { name: 'Next' }).click();

    await page.locator('//*[@id="exportStandardOptionsDialog"]/ui-dialog-footer/button[1]').click();
  
    await page.getByText('Show export statistics');
  
    const [download] = await Promise.all([
      page.waitForEvent('download'),
      page.locator('#webExportReportDialog > ui-dialog-contents > p:nth-child(1) > a').click()
    ]);
  
    await download.saveAs('game-export.zip');
  
    await page.getByText('Show export statistics').click();
    await page.waitForSelector('#exportStatsDialog > ui-dialog-contents');
    let stats: string = await page.evaluate("Array.from(document.querySelector('#exportStatsDialog > ui-dialog-contents').children, ({innerHTML}) => innerHTML.trim()).filter(Boolean).join('\\n')");
    stats = stats.replace(/<\/div>/ig, '\n');
    stats = stats.replace(/<\/li>/ig, '\n');
    stats = stats.replace(/<li>/ig, '  *  ');
    stats = stats.replace(/<\/ul>/ig, '\n');
    stats = stats.replace(/<\/p>/ig, '\n');
    stats = stats.replace(/<br\s*[\/]?>/gi, "\n");
    stats = stats.replace(/<[^>]+>/ig, '');
  
    console.log(stats);
    writeFile('stats.txt', stats, function (err) {
      if (err) 
        console.log(err);
    });
  });
});